#!/usr/bin/env python3
"""
Very simple HTTP file server in python
Usage::
    ./server.py [<port>]
Authors::
    fabiand, orgads, TijlE-1951707 (https://gist.github.com/fabiand/5628006)
"""
from http.server import BaseHTTPRequestHandler, HTTPServer, ThreadingHTTPServer
import mimetypes
import os
import pathlib

# # https://stackoverflow.com/questions/62599036/python-requests-is-slow-and-takes-very-long-to-complete-http-or-https-request
# import logging
# import http.server
# BaseHTTPRequestHandler=http.server.BaseHTTPRequestHandler
# HTTPServer=http.server.HTTPServer
# http.server.BaseHTTPRequestHandler.debuglevel = 1
# http.server.HTTPServer.debuglevel = 1
# logging.basicConfig()
# logging.getLogger().setLevel(logging.DEBUG)

cwd = os.path.normcase(os.getcwd())
print(cwd)

class S(BaseHTTPRequestHandler):
    def __init__(self, *args, directory=None, **kwargs):
        self.protocol_version = 'HTTP/1.1'
        super().__init__(*args, **kwargs)

    def _send_response(self, code = 200, msg='Done', content_type='text/plain'):
        body = msg.encode('utf-8')
        self.send_response(code)
        self.send_header('Content-type', content_type)
        self.send_header('Content-Length', len(body))
        # self.send_header('Keep-Alive', "timeout=5")
        # self.send_header('Connection', "keep-alive")
        self.send_header('Cache-Control', "no-store") # seemed to be necessary to GET through axios, and added in GET through Insomnia
        # self.send_header('Cache-Control', "no-cache, no-store, must-revalidate") # is all that Insomnia added to custom requests
        self.end_headers()
        self.wfile.write(body)

    def req_path(self):
        fname = self.path[1:] # Strip leading slash
        path = os.path.normcase(os.path.dirname(os.path.realpath(fname)))
        if os.path.commonpath((path, cwd)) == cwd:
            return fname.replace("%20", " ")
        raise Exception("Access denied")

    def do_GET(self):
        try:
            path = self.req_path()
            mime_type = mimetypes.guess_type(self.req_path())
            content_type = ('text/plain' if (t:= mime_type[0]) == None else t) + ('' if (e := mime_type[1]) == None else f"; charset={e}")
            with open(path, "r") as src:
                self._send_response(200, src.read(), content_type)
        except FileNotFoundError as ex:
            self._send_response(404, '404 Not Found\r\n')
        except BrokenPipeError as ex:
            print("client disconnected")
            print(ex)
        except Exception as ex:
            print(ex, type(ex))
            self._send_response(500, 'Internal server error\r\n')

    def do_PUT(self):
        try:
            path = self.req_path()
            pathlib.Path(path).parent.mkdir(parents=True, exist_ok=True)
            with open(path, "wb") as dst:
                content_length = int(self.headers['Content-Length'])
                dst.write(self.rfile.read(content_length))
            self._send_response(200, 'Done\r\n')
        except Exception as ex:
            print(ex)
            self._send_response(403, '403 Access Denied\r\n')

    # def do_POST(self):
    #     print("infinite loop?")
    #     return self.do_PUT()

def run(port=8000):
    server_address = ('', port)
    # httpd = HTTPServer(server_address, S) # WARNING: if the server is busy, it will completely miss incoming requests!
    httpd = ThreadingHTTPServer(server_address, S)
    print(f'Listening on port {port}')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()

if __name__ == '__main__':
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
