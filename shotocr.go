package main

import (
	"bytes"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"os"
	"os/exec"
)

func main() {
	img, err := takeScreenshot()
	if err != nil {
		fmt.Println("Error taking screenshot:", err)
		return
	}
	//err = saveImage(outputFile, cropRightHalf(img))
	err = pipeImage(cropRightHalf(img))
	if err != nil {
		fmt.Println("Error pipe image:", err)
		return
	}
}

func pipeImage(img image.Image) error {
	b := new(bytes.Buffer)
	if err := png.Encode(b, img); err != nil {
		fmt.Println("image to bytes", err)
		return err
	}
	// https://github.com/tesseract-ocr/tesseract/releases/
	cmd := exec.Command("tesseract", "stdin", "-", "-l", "eng")
	cmd.Stdin = b
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		fmt.Println("pipe CLI", err)
		return err
	}
	return nil
}

func takeScreenshot() (image.Image, error) {
	//cmd := exec.Command("adb", "shell", "screencap", "/sdcard/screenshot.png")
	cmd := exec.Command("adb", "exec-out", "screencap", "-p")
	b := new(bytes.Buffer)
	cmd.Stdout = b
	if err := cmd.Run(); err != nil {
		return nil, err
	}

	if img, _, err := image.Decode(b); err != nil {
		return nil, err
	} else {
		return img, nil
	}
}

func readImage(filename string) (image.Image, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	img, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}
	return img, nil

}

func saveImage(filename string, img image.Image) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	// Determine the format of the original image
	format := "png" // or "jpeg" based on your original screenshot format
	if format == "png" {
		err = png.Encode(file, img)
	} else {
		err = jpeg.Encode(file, img, nil)
	}

	if err != nil {
		return err
	}

	return nil
}

func cropRightHalf(img image.Image) image.Image {
	bounds := img.Bounds()
	width := bounds.Max.X
	height := bounds.Max.Y

	// Crop the right half
	croppedWidth := width / 2
	croppedImg := img.(interface {
		SubImage(r image.Rectangle) image.Image
	}).SubImage(image.Rect(croppedWidth, 0, width, height))

	return croppedImg
}
