import sys
import requests
from time import time
from random import choice
from PyQt5.QtCore import QBasicTimer
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QTextBrowser, QGridLayout


class Demo(QWidget):
    def __init__(self):
        super().__init__()
        self.err_label = QLabel(self)
        self.quote_content = QTextBrowser(self)
        self.quote_author = QTextBrowser(self)
        self.refresh_button = QPushButton('Refresh', self)
        self.test_button.setCheckable(False)
        self.test_button.clicked.connect(self.refresh_quote)

        # background thread checks this ts, to update every few minutes
        self.last_update_timestamp = 0

        # every few min thread in timer
        self.jump_time = tuple(range(40, 400, 30))
        QBasicTimer().start(2000, self)
        self.init_layout()

    def init_layout(self):
        '''
           c0 c1 c2 c3 c4 c5 c6 c7
        r0 |1 |1 |1 |1 |1 |1 |1 |
        r1 |1 |  |  |  |  |1 |1 |
        r2 |1 |  |  |  |  |1 |1 |
        r3 |1 | 1| 1| 1|1 | 1|1 |
        r4 |  |  |  |  |  |  |  |
        r5 |  |  |  |2 |2 |2 |2 |
        r6 |  |  |  |2 |2 |2 |2 |
        '''
        grid_layout = QGridLayout()
        grid_layout.addWidget(self.quote_content, 0, 0, 4, 8)
        grid_layout.addWidget(self.quote_author, 5, 4, 2, 4)
        grid_layout.addWidget(self.err_label, 8, 0)

    def timerEvent(self, *args, **kw):
        if time() >= self.last_update_timestamp + choice(self.jump_time):
            self.refresh_quote()

    def refresh_quote(self):
        self.err_label.setText('')
        try:
            quote_req = requests.get('http://api.quotable.io/random', timeout=5)
            quote_req.raise_for_status()
            quote_json = quote_req.json()
            self.quote_author.setText(quote_json['author'])
            self.quote_content.setText(quote_json['content'])
            self.last_update_timestamp = time()
        except Exception as e:
            self.err_label.setText(f'[ERROR] {e}')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    demo = Demo()
    demo.show()
    sys.exit(app.exec_())

