/* -lX11 */
#include<stdio.h>
#include<time.h>
#include<unistd.h>

#include <X11/XKBlib.h>
#include <X11/Xlib.h>

static Display *dpy;
static int screen;
static Window root;

void c(char* n, char* p, int m) {
    FILE* f = fopen(n, "r");
    int bad = 1;
    if (f) {
        if (NULL != fgets(p, m, f)) {
            bad = 0;
        }
        fclose(f);
    }
    if (bad) {
        p[0] = 'E';
        p[1] = 0;
    }
}
char c1(char* n) {
    FILE* f = fopen(n, "r");
    char ret = 'E';
    if (f) {
        if ((ret = getc(f)) != EOF) {
            switch (ret) {
                case 'D': ret = '-'; break;
                case 'F': ret = '='; break;
                case 'C': ret = '+'; break;
                default: ret = '?';
            }
        }
        fclose(f);
    }
    return ret;
}

void tmf(char* r) {
    /* "Tu 07-19 21:04:12" 17+1=18 */
    static const char wday_name[][3] = { "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" };
    time_t rawtime;
    struct tm * timeptr;
    time(&rawtime);
    timeptr = localtime(&rawtime);
    sprintf(r, "%s %02d-%02d %02d:%02d:%02d", wday_name[timeptr->tm_wday],
        timeptr->tm_mon+1,timeptr->tm_mday,timeptr->tm_hour, timeptr->tm_min, timeptr->tm_sec);
}

char capAa() {
    unsigned n;
    XkbGetIndicatorState(dpy, XkbUseCoreKbd, &n);
    return (n & 1)?'A':'a';
}

int main() {
    dpy = XOpenDisplay(NULL);
    if(!dpy)
        return 1;
    screen = DefaultScreen(dpy);
    root = RootWindow(dpy, screen);

    /*  0123456789 */
    /* "98-  38 A Tu 07-19 21:04:12" */
    /* "100- 38 A Tu 07-19 21:04:12" 27+1=28 */
    char evth[] = { [4] = ' ', [9] = ' ', [27] = 0 };

    while(1) {
        c("/sys/class/power_supply/BAT0/capacity", evth, 4);
        /* 0123    01234 */
        /* XY0  -> XYs0  */
        /* 1MN0 -> 1MNs0 */
        int s = 2;
        if (evth[2] < '0') {
            evth[3] = ' ';
        } else {
            s = 3;
        }
        evth[s] = c1("/sys/class/power_supply/BAT0/status");

        c("/sys/class/thermal/thermal_zone0/temp", evth+5, 3);
        evth[7] = ' ';

        evth[8] = capAa();

        tmf(evth+10);

        XStoreName(dpy, root, evth);
        sleep(1);
    }

    /* TODO sig */
    XCloseDisplay(dpy);
    return 0;
}
