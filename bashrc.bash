[[ $- != *i* ]] && return

if [ -z "$DISPLAY" ]; then
    if pgrep "xfce4" > /dev/null 
    then
        echo "xfce started elsewhere"
    else
        #sudo /usr/local/bin/tailscaled &
        sudo LANG=en_UK.UTF8 startxfce4
    fi
fi

# alias ls='ls --color=auto'
# PS1='[\u@\h \W]\$ '
if [[ $MAGICVAR == magical ]]; then
    #curl ipinfo.io &
    exec /usr/lib/firefox/firefox -no-remote -P magicprofile
fi
