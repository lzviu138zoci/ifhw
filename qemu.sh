# live
qemu-system-x86_64 -enable-kvm -cpu host -smp 2 -m 8192M \
-device virtio-net,netdev=vmnic,mac=12:12:12:12:12:12 \
-netdev user,id=vmnic,hostfwd=tcp::8000-:8000 \
-drive file=/path/os-live.iso,media=cdrom -boot d -display gtk -vga std
# -net nic 
#-netdev user,id=vmnic,hostfwd=tcp::10022-:22