def luhn(v: int):
  q, c = divmod(v, 10)
  s = True
  sum = 0
  while q:
    q, d = divmod(q, 10)
    if s:
      d *= 2
      sum += d if d < 10 else (d%10 + 1)
    else:
      sum += d
    s = not s
  assert c == (-sum % 10)

def cnid():
  a = (7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2)
  print('10X98765432'[sum(int(c) * a[i] for i, c in enumerate(input('first 17 digits: '))) % 11])