#include<stdio.h>
#include<stdlib.h>

#define ERROR_ARGS 1
#define ERROR_FP_OPEN 2
#define ERROR_MAGIC 3
#define ERROR_VERSION_READ 4
#define ERROR_CIPHER_UNSUPPORTED 5

const size_t ucharSize = sizeof(unsigned char);

int magicCheck(FILE *fp) {
    static const unsigned char sig[] = {3, 217, 162, 154, 103, 251, 75, 181};
    for (int i=0; i<8; ++i) {
        if (sig[i] != fgetc(fp)) {
            return 1;
        }
    }
    return 0;
}

int verisonRead(FILE *fp) {
    unsigned int versionNum;
    if (1 != fread(&versionNum, sizeof(unsigned int), 1, fp)) {
        return 1;
    }
    printf("version number = 0X%X\n", versionNum);
    return 0;
}

/**
 * check head item for AES, retCode -= 1 if passed
 */
void checkCipherAES(unsigned char *dataP, unsigned char *codeP) {
    static const unsigned char sig[] = {0x31, 0xC1, 0xF2, 0xE6, 0xBF, 0x71,
        0x43, 0x50, 0xBE, 0x58, 0x05, 0x21, 0x6A, 0xFC, 0x5A, 0xFF};
    for (int i=0; i<16; ++i) {
        if (sig[i] != dataP[i]) {
            puts("head says not AES");
            return;
        }
    }
    *codeP -= 1;
}

/**
 * head item for salsa20 is \x02\x00\x00\x00, retCode -= 2 if passed
 */
void checkInnerSalsa20(unsigned char *dataP, unsigned char *codeP) {
    for (int i=0; i<4; ++i) {
        if (i?0:2 != dataP[i]) {
            puts("head says not Salsa20");
            return;
        }
    }
    *codeP -= 2;
}

int headCheck(FILE *fp) {
    unsigned char headIndex,
                  retCode = 3, // 2 checks (3-2-1=0)
                  *dataPtr = NULL; // to hold data[headLen]
    unsigned short headLen, // len of current head item
                   maxLen = 0; // max len ever, to spare malloc
    for (;;) {
        if (1 == fread(&headIndex, ucharSize, 1, fp) && // read index & len
            1 == fread(&headLen, sizeof(unsigned short), 1, fp)) {
            if (dataPtr == NULL || headLen > maxLen) { // need mem alloc
                // printf("upd head item len from %d to %d\n", maxLen, headLen);
                maxLen = headLen;
                dataPtr = (unsigned char *) realloc(dataPtr, maxLen * ucharSize);
                if (dataPtr == NULL) {
                    puts("mem exhausted when head checking");
                    return 99;
                }
            }
            if (headLen != fread(dataPtr, ucharSize, headLen, fp)) {
                puts("data exhausted when head checking");
                return 98;
            }
            switch (headIndex) {
                case 0:
                    if (dataPtr) {
                        free(dataPtr);
                    }
                    return retCode;
                case 2:
                    checkCipherAES(dataPtr, &retCode);
                    break;
                case 10:
                    checkInnerSalsa20(dataPtr, &retCode);
                    break;
                // TODO other head cases OUT for later usage
            }
        } else {
            puts("head item garbled when head checking");
            return 97;
        }
    }
}

int main(int argc, char *argv[]) {
    // for (int i=0; i<argc; ++i) {
    //     printf("%d %s; ", i, argv[i]);
    // }
    // putchar('\n');
    if (argc == 1) {
        puts("no kdbx, bye");
        return ERROR_ARGS;
    }
    FILE *fp = NULL;
    fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("open \"%s\" failed, bye\n", argv[1]);
        return ERROR_FP_OPEN;
    } else {
        printf("opened \"%s\"\n", argv[1]);
    }
    if (magicCheck(fp)) {
        puts("MAGIC CHECK BAD");
        fclose(fp);
        return ERROR_MAGIC;
    } // else { puts("HEAD CHECK GOOD"); }
    if (verisonRead(fp)) {
        puts("VERSION READ BAD");
        fclose(fp);
        return ERROR_VERSION_READ;
    } // else { puts("HEAD CHECK BAD"); }
    if (headCheck(fp)) {
        puts("HEAD CIPHER BAD");
        fclose(fp);
        return ERROR_CIPHER_UNSUPPORTED;
    }
    fclose(fp);
    return 0;
}
