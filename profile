export PS1="\[\033[1;32;40m\]\[\033[0;37;40m\]\[\033[34;40m\]\[\033[1;31;40m\]\u\[\033[0;34;40m\]\[\033[0;37;40m\]@\[\033[35;40m\]\w\$([ \$? = 0 ] && printf '\[\033[1;32;40m\]' || printf '\[\033[1;33;40m\]')\\$\[\033[0m\] "
export XDG_CACHE_HOME=/tmp/$USER
export XDG_DATA_HOME=/tmp/$USER
[ -d $XDG_DATA_HOME ] || mkdir -m 700 -p $XDG_DATA_HOME

# zsh convention @ ~/.zprofile
if test -z "$XDG_RUNTIME_DIR"; then
    export XDG_RUNTIME_DIR=$(mktemp -d /tmp/$(id -u)-runtime-dir.XXX)
fi